package com.tiketka;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TiketKA {
    private JRadioButton bisnisRadioButton;
    private JRadioButton ekonomiRadioButton;
    private JComboBox comboBoxJurusan;
    private JTextField tfHarga;
    private JPanel TiketPanel;
    private JTextField tfNama;
    private JTextField tfJumlah;
    private JTextField tfTotal;
    private JTextField tfBayar;
    private JTextField tfKembalian;
    private JButton cetakButton;
    private JButton resetButton;
    private JTextArea taCetak;
    private ButtonGroup kelas;
    int bisnis, ekonomi, beli, harga, total, bayar, kembali;

    public TiketKA() {
        kelas = new ButtonGroup();
        kelas.add(bisnisRadioButton);
        kelas.add(ekonomiRadioButton);

        comboBoxJurusan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (comboBoxJurusan.getSelectedItem().equals("Pilih Jurusan")) {
                    kelas.clearSelection();
                    tfHarga.setText("");
                    bisnis = 0;
                    ekonomi = 0;
                } else if (comboBoxJurusan.getSelectedItem().equals("Yogyakarta")) {
                    bisnis = 50000;
                    ekonomi = 42000;
                } else if (comboBoxJurusan.getSelectedItem().equals("Purworejo")) {
                    bisnis = 30000;
                    ekonomi = 25000;
                } else if (comboBoxJurusan.getSelectedItem().equals("Semarang")) {
                    bisnis = 55000;
                    ekonomi = 47000;
                }
            }
        });
        bisnisRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (bisnisRadioButton.isSelected()) {
                    tfHarga.setText(String.valueOf(bisnis));
                }
            }
        });
        ekonomiRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ekonomiRadioButton.isSelected()) {
                    tfHarga.setText(String.valueOf(ekonomi));
                }
            }
        });
        tfJumlah.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                beli = Integer.parseInt(tfJumlah.getText());
                harga = Integer.parseInt(tfHarga.getText());
                total = bayar * harga;
                tfTotal.setText(String.valueOf(total));
            }
        });
        tfBayar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bayar = Integer.parseInt(tfBayar.getText());
                kembali = Integer.parseInt(tfTotal.getText());
                total = bayar - kembali;
                tfKembalian.setText(String.valueOf(total));
            }
        });
        tfJumlah.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int a,b,c;
                a = Integer.parseInt(tfHarga.getText());
                b = Integer.parseInt(tfJumlah.getText());
                c = a * b;
                tfTotal.setText(""+c);
            }
        });
        tfBayar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int a,b,c;
                a = Integer.parseInt(tfBayar.getText());
                b = Integer.parseInt(tfTotal.getText());
                c = a - b;

                if (a < b){
                    tfKembalian.setText("Uang Kurang");
                }else{
                    tfKembalian.setText(""+c);
                }

            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kelas.clearSelection();
                comboBoxJurusan.setSelectedItem("Pilih Jurusan");
                tfHarga.setText("");
                tfNama.setText("");
                tfTotal.setText("");
                tfJumlah.setText("");
                tfBayar.setText("");
                tfKembalian.setText("");
                taCetak.setText("");
            }
        });
        cetakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taCetak.setText(
                        " Nama Penumpang :   "+tfNama.getText()+"\n"+
                        " Jurusan Kereta        :   "+comboBoxJurusan.getSelectedItem()+"\n"+
                        " Jumlah Beli              :   "+tfJumlah.getText()+"  Tiket\n"+
                        " Total Bayar               :   Rp."+tfTotal.getText()+"\n"+
                        " Uang Bayar              :   Rp."+tfBayar.getText()+"\n"+
                        " Uang Kembali         :   Rp."+tfKembalian.getText()+"\n");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TiketKA");
        frame.setContentPane(new TiketKA().TiketPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}